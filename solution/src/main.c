#include "../include/_format_bmp.h"
#include "../include/file_util.h"
#include "../include/image_transformer.h"

int main( int argc, char** argv ) {
    if (argc != 4) {
        fprintf(stderr, "Invalid arguments provided. Expected: 3, provided: %d\n", argc - 1);
        return 1;
    }

    char* source_img = argv[1];
    char* destination_img = argv[2];
    int64_t target_angle = strtoll(argv[3], NULL, 10);

    FILE* bmp_file;
    enum file_open_status source_open_result = open_file(source_img, "r", &bmp_file);
    if (source_open_result != OPEN_OK) {
        fprintf(stderr, "Failed to open source img file. Open error code: %d\n", source_open_result);
        return 1;
    }

    struct image img;
    enum read_status read_result = from_bmp(bmp_file, &img);
    if (read_result != READ_OK) {
        fprintf(stderr, "Failed to read bmp. BMP read error code: %d\n", read_result);
        return 1;
    }

    struct image rotated;
    enum rotation_status rotation_result = rotate(&img, &rotated, target_angle);
    if (rotation_result != ROTATION_OK) {
        fprintf(stderr, "Failed to rotate image. Rotation error code: %d\n", rotation_result);
        return 1;
    }

    FILE* result_bmp_file;
    enum file_open_status destination_open_result = open_file(destination_img, "w", &result_bmp_file);
    if (destination_open_result != OPEN_OK) {
        fprintf(stderr, "Failed to open destination img file. Open error code: %d\n", destination_open_result);
        return 1;
    }

    enum write_status write_result = to_bmp(result_bmp_file, &rotated);
    if (write_result != WRITE_OK) {
        fprintf(stderr, "Failed to write result. BMP write error code: %d\n", write_result);
        return 1;
    }

    image__free(&img);
    if (target_angle != 0)
        image__free(&rotated);

    enum file_close_status source_close_result = close_file(bmp_file);
    if (source_close_result != CLOSE_OK) {
        fprintf(stderr, "Failed to close source file. Close error code: %d\n", source_close_result);
        return 1;
    }

    enum file_close_status destination_close_result = close_file(result_bmp_file);
    if (destination_close_result != CLOSE_OK) {
        fprintf(stderr, "Failed to close destination file. Close error code: %d\n", destination_close_result);
        return 1;
    }
    
    return 0;
}
