#include "../include/_format_bmp.h"

#define FILETYPE_CONST 0x4D42
#define BITS_PER_PIXEL 24
#define COLOR_PLANES 1
#define HEADER_SIZE_BYTES 40

/**
 * This function calculates image padding.
 * 
*/
long calculate_padding(struct image* img) {
    return (long) ((4 - ((sizeof(struct pixel) * img->width) % 4)) % 4);
}

/**
 * This function reads BMP image from file.
 * 
 * @param input_file BMP image file
 * @param img image destination ptr
 * @returns read status. Depends on the operation result
*/
enum read_status from_bmp(FILE* input_file, struct image* img) {
    // check if image input file ptr is null
    if (input_file == NULL)
        return READ_INVALID_FILE_PTR;

    // check if image destination ptr is null
    if (img == NULL)
        return READ_INVALID_DESTINATION_PTR;
    
    struct bmp_header header;
    size_t header_read_result = fread(&header, sizeof(struct bmp_header), 1, input_file);

    // check if header read operation not succeed
    if (header_read_result != 1)
        return READ_INVALID_FILE_TYPE;

    // check if file type is not .bmp
    if (header.bfType != 0x4D42)
        return READ_INVALID_FILE_TYPE;

    struct image read_image = image__create(header.biHeight, header.biWidth);

    // check if allocation not succeed
    if (read_image.data == NULL)
        return READ_NOT_ENOUGH_SPACE;

    for (uint32_t i = 0; i < read_image.height; ++i) {
        size_t pixel_row_read_result = fread(&read_image.data[i * read_image.width], sizeof(struct pixel), read_image.width, input_file);
        int pixel_row_padding_skip_result = fseek(input_file, calculate_padding(&read_image), SEEK_CUR);

        // check if pixel row read succeed
        if (pixel_row_read_result != read_image.width || pixel_row_padding_skip_result == -1)
            return READ_INVALID_DATA;
    }

    *img = read_image;
    return READ_OK;
}

/**
 * This function writes BMP image to file.
 *  
 * @param output_file BMP image file
 * @param img image source ptr
 * @returns write status. Depends on the operation result
*/
enum write_status to_bmp(FILE* output_file, struct image* img) {
    // check if image output file ptr is null
    if (output_file == NULL)
        return WRITE_ERROR_FILE_PTR;
    
    // check if image source ptr is null
    if (img == NULL)
        return WRITE_ERROR_SOURCE_PTR;
    
    uint32_t file_size = sizeof(struct bmp_header) + sizeof(struct pixel) * img->width * img->height;
    struct bmp_header header = {
        .bfType = FILETYPE_CONST,
        .bfileSize = file_size,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = HEADER_SIZE_BYTES,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = COLOR_PLANES,
        .biBitCount = BITS_PER_PIXEL
    };

    size_t header_write_result = fwrite(&header, sizeof(struct bmp_header), 1, output_file);
    // check if header write failed
    if (header_write_result != 1)
        return WRITE_ERROR_HEADER;


    for (uint32_t i = 0; i < img->height; ++i) {
        size_t pixels_row_write_result = fwrite(&img->data[img->width * i], sizeof(struct pixel), img->width, output_file);
        int pixels_row_padding_skip_result = fseek(output_file, calculate_padding(img), SEEK_CUR);

        // check if pixel row write succeed
        if (pixels_row_write_result != img->width || pixels_row_padding_skip_result == -1)
            return WRITE_ERROR_IMG_DATA;
    }

    return WRITE_OK;
}
