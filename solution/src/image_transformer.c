#include "../include/image_transformer.h"

/**
 * This function rotates image 90 degrees left.
 *  
 * @param source source image
 * @param destination destination img
 * @returns rotation status
*/
enum rotation_status rotate_90_left(struct image* source, struct image* destination) {
    // check if source ptr is null
    if (source == NULL)
        return ROTATION_INVALID_SOURCE_IMG;
    
    // check if destination ptr is null
    if (destination == NULL)
        return ROTATION_INVALID_DESTINATION_IMG;

    // create new image
    struct image new_img = image__create(source->width, source->height);
    // check if image pixels space was not allocated
    if (new_img.data == NULL)
        return ROTATION_NOT_ENOUGH_SPACE;

    // rotate existing image 90 degrees left
    for (uint32_t i = 0; i < source->height; ++i) {
        for (uint32_t j = 0; j < source->width; ++j) {
            new_img.data[j * new_img.width + (new_img.width - i - 1)] = source->data[i * source->width + j];
        }
    }

    *destination = new_img;
    return ROTATION_OK;
}


/**
 * This function performs image rotation provided amount of times.
 * 
 * @param source image to be rotated ptr 
 * @param destination rotated img ptr
 * @param times rotation times
 * @returns rotation status
*/
enum rotation_status rotate_times(struct image* source, struct image* destination, size_t times) {
    // check if source img ptr is null
    if (source == NULL)
        return ROTATION_INVALID_SOURCE_IMG;

    // check if destination img ptr is null
    if (destination == NULL)
        return ROTATION_INVALID_DESTINATION_IMG;
    
    // if rotation times is 1 -> excessive memory deallocation is not required
    if (times == 1)
        return rotate_90_left(source, destination);
    
    struct image rotated;
    rotate_90_left(source, &rotated);

    struct image buffer = rotated;
    for (size_t i = 1; i < times; ++i) {
        
        enum rotation_status rot_result = rotate_90_left(&buffer, &rotated);

        if (rot_result != ROTATION_OK)
            return rot_result;

        image__free(&buffer);
        buffer = rotated;
    }

    *destination = rotated;
    return ROTATION_OK;
}

/**
 * This function rotates image.
 * 
 * @param source image to be rotated
 * @param destination rotated image
 * @param angle rotation angle
 * @returns rotation status
*/
enum rotation_status rotate(struct image* source, struct image* destination, int64_t angle) {
    // check if source img ptr is null
    if (source == NULL)
        return ROTATION_INVALID_SOURCE_IMG;

    // check if destination img ptr is null
    if (destination == NULL)
        return ROTATION_INVALID_DESTINATION_IMG;
    
    // if rotation is not required
    if (angle == 0) {
        *destination = *source;
        return ROTATION_OK;
    }
    
    switch (angle) {
        case -90:
        case 270:
            return rotate_times(source, destination, 1);
        case -180:
        case 180:
            return rotate_times(source, destination, 2);
        case -270:
        case 90:
            return rotate_times(source, destination, 3);
        default:
            return ROTATION_INVALID_ANGLE;
    }
}
