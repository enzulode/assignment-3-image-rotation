#include "../include/file_util.h"

/**
 * This function opens file by provided name.
 *  
 * @param filename file name
 * @param mode file open mode
 * @param file ptr on opened file ptr
 * @returns file open status
*/
enum file_open_status open_file(char* filename, char* mode, FILE** file) {
    // check if filename ptr is null
    if (filename == NULL)
        return OPEN_INVALID_FILENAME;

    // check if open mode ptr is null
    if (mode == NULL)
        return OPEN_INVALID_MODE;
    
    FILE* opened_file = fopen(filename, mode);

    // check if file open failed
    if (opened_file == NULL)
        return OPEN_FAILED;
    
    *file = opened_file;
    return OPEN_OK;
}

/**
 * This function closes opened file.
 *  
 * @param file file to be closed
 * @returns file close status
*/
enum file_close_status close_file(FILE* file) {
    // check if file ptr is null
    if (file == NULL)
        return CLOSE_INVALID_FILE;
    
    int close_result = fclose(file);
    if (close_result != 0)
        return CLOSE_FAILED;

    return CLOSE_OK;
}

