#include "../include/_internal_format.h"

/**
 * This function creates image with specific props.
 *  
 * @param height image height in pixels
 * @param width image width in pixels
 * @returns created image
*/
struct image image__create(uint32_t height, uint32_t width) {
    return (struct image) {
        .height = height,
        .width = width,
        .data = malloc(sizeof(struct pixel) * height * width)
    };
}

/**
 * This function frees an existing image.
 *  
 * @param img existing image ptr
*/
void image__free(struct image* img) {
    // check if img ptr is null or img pixels ptr is null 
    if (img == NULL || img->data == NULL)
        return;
    
    free(img->data);
}
