#ifndef BMP_FORMAT
    #define BMP_FORMAT

    #include "_internal_format.h"
    
    #include <malloc.h>
    #include <stdio.h>
    #include <stdlib.h>

    /**
     * BMP format image header representation.
     * 
     * @param bfType BMP format magical const (0x4D42)
     * @param bfileSize file size in bytes
     * @param bfReserved reserved
     * @param bOffBits image data byte offset from the file beginning
     * @param biSize header size in bytes
     * @param biWidth image width in pixels
     * @param biHeight image height in pixels
     * @param biPlanes color planes number
     * @param biBitCount bit per pixel
     * @param biCompression compression type
     * @param biSizeImage image size in bytes
     * @param biXPelsPerMeter x-axis ppm
     * @param biYPelsPerMeter y-axis ppm
     * @param biClrUsed colors number
     * @param biClrImportant important colors
    */
    struct __attribute__((packed)) bmp_header
    {
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
    };

    /**
     * Possible read statuses.
     * 
    */
    enum read_status {
        READ_OK = 0,
        READ_INVALID_FILE_PTR,
        READ_INVALID_DESTINATION_PTR,
        READ_INVALID_FILE_TYPE,
        READ_NOT_ENOUGH_SPACE,
        READ_INVALID_DATA
    };

    /**
     * This function reads BMP image from file.
     * 
     * @param input_file BMP image file
     * @param img image destination ptr
     * @returns read status. Depends on the operation result
    */
    enum read_status from_bmp(FILE* input_file, struct image* img);

    /**
     * Possible write statuses.
     * 
    */
    enum write_status {
        WRITE_OK = 0,
        WRITE_ERROR_FILE_PTR,
        WRITE_ERROR_SOURCE_PTR,
        WRITE_ERROR_HEADER,
        WRITE_ERROR_IMG_DATA
    };

    /**
     * This function writes BMP image to file.
     * 
     * @param output_file BMP image file
     * @param img image source ptr
     * @returns write status. Depends on the operation result
    */
    enum write_status to_bmp(FILE* output_file, struct image* img);

#endif
