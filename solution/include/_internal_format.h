#ifndef INTERNAL_FORMAT
    #define INTERNAL_FORMAT
    
    #include <inttypes.h>
    #include <malloc.h>

    /**
     * Image pixel representation.
     * 
     * @param r pixel RED prop in RGB representation
     * @param g pixel GREEN prop in RGB representation
     * @param b pixel BLUE prop in RGB representation
    */
    struct pixel {
        uint8_t b, g, r;
    };

    /**
     * Internal image representation.
     * 
     * @param width image width in pixels
     * @param height image height in pixels
     * @param data image pixels array ptr
    */
    struct image {
        uint32_t width;
        uint32_t height;
        struct pixel* data;
    };

    /**
     * This function creates image with specific props.
     * 
     * @param height image height in pixels
     * @param width image width in pixels
     * @returns created image
    */
    struct image image__create(uint32_t height, uint32_t width);

    /**
     * This function frees an existing image.
     * 
     * @param img existing image ptr
    */
    void image__free(struct image* img);

#endif
