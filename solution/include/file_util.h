#ifndef FILE_UTILITY
    #define FILE_UTILITY

    #include <stdio.h>

    /**
     * This enum represents file open statuses.
     * 
    */
    enum file_open_status {
        OPEN_OK = 0,
        OPEN_INVALID_FILENAME,
        OPEN_INVALID_MODE,
        OPEN_FAILED
    };

    /**
     * This function opens file by provided name.
     * 
     * @param filename file name
     * @param mode file open mode
     * @param file ptr on opened file ptr
     * @returns file open status
    */
    enum file_open_status open_file(char* filename, char* mode, FILE** file);

    /**
     * This enum represents file close statuses.
     *
    */
    enum file_close_status {
        CLOSE_OK = 0,
        CLOSE_INVALID_FILE,
        CLOSE_FAILED
    };

    /**
     * This function closes opened file.
     * 
     * @param file file to be closed
     * @returns file close status
    */
    enum file_close_status close_file(FILE* file);

#endif
