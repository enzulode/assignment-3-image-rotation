#ifndef IMAGE_TRANSFORMER
    #define IMAGE_TRANSFORMER

    #include "_internal_format.h"

    /**
     * This enum represents rotation permutation status.
     * 
    */
    enum rotation_status {
        ROTATION_OK = 0,
        ROTATION_INVALID_SOURCE_IMG,
        ROTATION_INVALID_DESTINATION_IMG,
        ROTATION_NOT_ENOUGH_SPACE,
        ROTATION_INVALID_ANGLE
    };

    /**
     * This function rotates image 90 degrees left.
     * 
     * @param source source image
     * @param destination destination img
     * @returns rotation status
    */
    enum rotation_status rotate_90_left(struct image* source, struct image* destination);

    /**
     * * This function rotates image.
     *  
     * @param source image to be rotated
     * @param destination rotated image
     * @param angle rotation angle
     * @returns rotation status
    */
    enum rotation_status rotate(struct image* source, struct image* destination, int64_t angle);

#endif
